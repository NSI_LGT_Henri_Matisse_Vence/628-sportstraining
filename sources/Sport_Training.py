import tkinter as tk
from tkinter import ttk,messagebox,font
import webbrowser
import requests
import folium
from geopy.geocoders import Photon
# Cette fonction permet de calculer l'IMC de l'utilisateur afin de l'utiliser ensuite.
def calculer_imc():
#On demande a l'utilisateur sa taille et son poids puis on calcule son IMC.
    poids = float(entry_poids.get())
    taille = float(entry_taille.get()) / 100
    imc = poids / (taille ** 2)
    interpretation = interprete_imc(imc)
#Ensuite on commence a créer l'interface en demandant l'âge de l'utilisateur,le niveau d'activité, le sexe et la ville puis on calcule son besoin calorique.
    age = int(entry_age.get())
    niveau_activite = combo_niveau_activite.get()
    sexe = combo_sexe.get()
    ville = entry_adresse.get()
    besoin_calorique = calculer_besoins_caloriques(poids, taille, age, niveau_activite, sexe)
# Ensuite on définie ce que le programme va renvoyer a l'utilisateur sur l'interface par rapport a son IMC, son besoin calorique arrondi a l'unité et sa ville.
    resultat_imc = f"Votre IMC est : {imc:.2f} ({interpretation})\n"
    resultat_besoin_calorique = f"Besoin calorique estimé : {besoin_calorique} cal/jour\n"
    resultat_adresse = f"Vous recherchez dans la ville de {ville}."

    label_resultat.config(text=resultat_imc + resultat_besoin_calorique + resultat_adresse)
# Cette fonction utilise l'IMC de l'utilisateur qui a été calculer ultérieurement puis suivant son IMC cette fonction va lui dire si il est en surpoids par exemple.
def interprete_imc(imc):
    if imc < 18.5:
        return "Insuffisance pondérale"
    elif 18.5 <= imc < 25:
        return "Poids normal"
    elif 25 <= imc < 30:
        return "Surpoids"
    elif 30 <= imc < 35:
        return "Obésité modérée"
    elif 35 <= imc < 40:
        return "Obésité sévère"
    else:
        return "Obésité morbide"

def calculer_besoins_caloriques(poids, taille, age, niveau_activite, sexe):
    '''
    On détermine le facteur d'activité en fonction du niveau d'activité donné
    '''
    if niveau_activite == "Faible(0-1/semaine)":
        facteur_activite = 1.2
    elif niveau_activite == "Moyen(2-3/semaine)":
        facteur_activite = 1.3
    elif niveau_activite == "Élevé(3 ou plus/semaine)":
        facteur_activite = 1.4
    else:
        facteur_activite = 1.2

    if sexe == "Femme":
        besoin_calorique = 447.593 + (9.247 * poids) + (3.098 * taille) - (4.33 * age)
    elif sexe == "Homme":
        besoin_calorique =  88.362 + (13.397 * poids) + (4.799 * taille) - 5.677 * age
    else:
        besoin_calorique = 88.362 + (13.397 * poids) + (4.799 * taille) - 5.677 * age
    besoin_calorique *= facteur_activite
    return round(besoin_calorique)
    label_resultat.config(text="Besoin calorique estimé: {:.2f} calories/jour".format(calories_necessaires))


def suggerer_entrainement(niveau_activite, poids):
    """
    Cette fonction va prendre en paramètre le niveau d'activité entré par l'utilisateur et son poids
    et va les utiliser pour lui renvoyer dans un onglet une suggestion d'entrainement adapter a lui puis
    en appuyant sur "OK" un lien va s'ouvrir pour montrer une video d'un ou plusieurs exercices a faire en
    fonction de sa suggestion.
    """
    if niveau_activite == "Faible(0-1/semaine)" and 0 < poids <= 50:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Commencez en douceur avec des activités légères pour renforcer votre énergie et votre vitalité comme de la marche rapide. Chaque petit pas compte dans votre parcours vers une meilleure forme physique! Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://conseilsport.decathlon.fr/comment-prendre-de-la-masse-musculaire-nutrition"))

    elif niveau_activite == "Faible(0-1/semaine)"and 50 < poids <= 70:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Commencez en douceur avec des activités légères pour stimuler votre corps. Des promenades régulières et des exercices simples peuvent être un excellent point de départ. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://www.trouver-un-cours.fr/blog/se-mettre-sport-conseils-debuter-rythme_134/#:~:text=D%C3%A9marrer%20progressivement&text=Commencez%20par%20des%20s%C3%A9ances%20de,marche%20rapide%20les%20premiers%20jours."))

    elif niveau_activite == "Faible(0-1/semaine)" and 70 < poids < 90:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Commencez votre entraînement en douceur avec des activités légères adaptées à votre niveau d'activité actuel et à votre poids. Des promenades régulières, la natation ou le yoga peuvent être d'excellentes options pour débuter. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://www.santemagazine.fr/minceur/coaching-minceur/conseils-pour-maigrir/peut-on-maigrir-grace-au-sport-177906"))

    elif niveau_activite == "Faible(0-1/semaine)" and poids >= 90:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Commencez en douceur en choisissant des activités adaptées à votre niveau. Des exercices de marche légère, de natation ou de yoga peuvent constituer un excellent point de départ. Progressivement, augmentez l'intensité pour améliorer votre santé et votre bien-être. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://www.santemagazine.fr/minceur/coaching-minceur/conseils-pour-maigrir/peut-on-maigrir-grace-au-sport-177906"))

    elif niveau_activite == "Moyen(2-3/semaine)" and 0 < poids <= 50:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Optez pour des exercices modérés qui stimuleront votre niveau d'activité. Des séances de cardio légères, comme la marche rapide ou le vélo, peuvent être parfaites pour maintenir votre forme, surtout avec un poids inférieur à 50 kilos. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://conseilsport.decathlon.fr/comment-prendre-de-la-masse-musculaire-nutrition"))

    elif niveau_activite == "Moyen(2-3/semaine)" and 50 < poids <= 70:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Pour maintenir votre niveau d'activité modéré, optez pour des exercices variés tels que la marche rapide, le vélo ou la danse. Une combinaison de cardio et de renforcement musculaire vous aidera à rester actif et en forme. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://conseilsport.decathlon.fr/programme-sport-a-la-maison-une-semaine-dexercices-faciles"))

    elif niveau_activite == "Moyen(2-3/semaine)" and 70 < poids < 90:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Maintenez votre niveau d'activité modéré avec des séances équilibrées. Intégrez des activités comme la marche rapide, le vélo ou des séances de musculation légère pour renforcer votre forme physique. Restez régulier et ajustez votre routine en fonction de vos progrès. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://www.santemagazine.fr/minceur/coaching-minceur/conseils-pour-maigrir/peut-on-maigrir-grace-au-sport-177906"))

    elif niveau_activite == "Moyen(2-3/semaine)" and poids >= 90:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Optez pour un entraînement modéré en intégrant des activités comme la marche rapide, la natation ou des exercices de musculation adaptés à votre poids. Variez vos séances pour maintenir la motivation et favoriser le bien-être physique. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://www.santemagazine.fr/minceur/coaching-minceur/conseils-pour-maigrir/peut-on-maigrir-grace-au-sport-177906"))

    elif niveau_activite == "Élevé(3 ou plus/semaine)" and 0 < poids <= 50:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", "Un programme d'entraînement idéal pour une personne active et pesant entre 0 et 50 kilos pourrait inclure une combinaison d'exercices de force tels que les squats, les pompes et les fentes pour renforcer les muscles, associés à des activités cardiovasculaires comme la course et la corde à sauter pour stimuler le système cardiorespiratoire. Des jours de repos actif avec de la marche rapide ou du yoga léger peuvent favoriser la récupération tout en maintenant l'activité physique. Il est crucial d'ajuster l'intensité et le volume selon les capacités individuelles et de consulter un professionnel de la santé pour un programme personnalisé. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://conseilsport.decathlon.fr/comment-prendre-de-la-masse-musculaire-nutrition"))

    elif niveau_activite == "Élevé(3 ou plus/semaine)" and 50 < poids <= 70:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", " un entraînement efficace pourrait inclure des séances de force comme les squats, développé couché et tractions, combinées à des exercices cardiovasculaires comme la course et la corde à sauter, alternant avec des jours de repos actif incluant de la marche rapide ou du yoga pour favoriser la récupération et maintenir la flexibilité. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://conseilsport.decathlon.fr/plans-dentrainement-musculation"))

    elif niveau_activite == "Élevé(3 ou plus/semaine)" and 70 < poids < 90:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", " Alternance entre séances de force (squats, développé couché, soulevé de terre) et séances cardio (course, vélo) trois fois par semaine, avec deux jours de repos ou de récupération active. Priorisez également la flexibilité et la mobilité avec des étirements réguliers. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://conseilsport.decathlon.fr/plans-dentrainement-musculation"))

    elif niveau_activite == "Élevé(3 ou plus/semaine)" and poids >= 90:
        messagebox.showinfo("Suggestion d'entraînement à faire chez soi", " Un programme d'entraînement adapté pourrait inclure des séances de musculation axées sur les exercices composés comme les squats, les soulevés de terre et les développés couchés, combinées à des séances de cardio intense telles que la course à intervalles et la corde à sauter. Un jour de repos actif avec de la marche ou du yoga pourrait favoriser la récupération tout en maintenant l'activité physique. Cliquer sur 'OK' afin d'ouvrir un lien pour un entrainement adapté.💪")
        print(webbrowser.open("https://www.santemagazine.fr/minceur/coaching-minceur/conseils-pour-maigrir/peut-on-maigrir-grace-au-sport-177906"))


def create_map(lat, lon):
    """
    Cette fonction va ouvrir google map avec les informations entrées par l'utilisateur dans l'interface Tkinter
    comme la ville et le sport que l'utilisateur veut pratiquer
    """
    m = folium.Map(location=[lat, lon], zoom_start=10)
    folium.Marker([lat, lon], tooltip="Votre adresse").add_to(m)
    m.save("map.html")
    webbrowser.open("map.html")

def ouvrir_map():
#Cette fonction fait fonctionner le boutton de l'interface " Ouvrir Map".
    adresse = entry_adresse.get(),combo_activite.get()
    webbrowser.open("https://www.google.fr/maps/search/\"{}\"".format(adresse))



fenetre = tk.Tk() # Crée l'interface.
fenetre.title("Sport Training") #Donne le titre de l'interface.
fenetre.geometry("800x550")  # Définie la taille de l'interface.

couleur_fond = "#D9EAD3"   # Donne la couleur du fond de l'interface.
couleur_texte = "#3C3C3C"  # Donne la couleur du texte de l'interface.
couleur_champ = "#FFFFFF"  # Donne la couleur des champs de l'interface.

fenetre.configure(bg=couleur_fond)
# Crée la signification de chaque champ.
label_poids = ttk.Label(fenetre, text="Poids (kg) :", background=couleur_fond, foreground=couleur_texte)
label_taille = ttk.Label(fenetre, text="Taille (cm) :", background=couleur_fond, foreground=couleur_texte)
label_age = ttk.Label(fenetre, text=" Age :", background=couleur_fond, foreground=couleur_texte)
label_lieu = ttk.Label(fenetre, text=" Type d'activite :", background=couleur_fond, foreground=couleur_texte)
label_niveau_activite = ttk.Label(fenetre, text="Niveau d'activité :", background=couleur_fond, foreground=couleur_texte)
label_adresse = ttk.Label(fenetre, text="Ville", background=couleur_fond, foreground=couleur_texte)
#Crée les différents champ.
entry_poids = ttk.Entry(fenetre, background=couleur_champ)
entry_taille = ttk.Entry(fenetre, background=couleur_champ)
entry_age = ttk.Entry(fenetre, background=couleur_champ)
entry_adresse = ttk.Entry(fenetre, background=couleur_champ)
# Défini par défaut les champs du poids de la taille et de l'âge.
entry_poids.insert(0, "  ")
entry_taille.insert(0, "  ")
entry_age.insert(0, "  ")
# Crée des listes de propositions pour le sexe, le sport et le niveau d'activité et crée les différents boutton de suggestion, de map et d'IMC.
label_sexe = ttk.Label(fenetre, text="Sexe :", background=couleur_fond, foreground=couleur_texte)
combo_sexe = ttk.Combobox(fenetre, values=["Homme", "Femme", "Non genré"], background=couleur_champ, foreground=couleur_texte)
combo_activite = ttk.Combobox(fenetre, values=["Tennis", "Padle", "natation","golf","stade de foot","fitness","Tir a l'arc","Danse","Basket","Kayak","stade de rugby","Dojo","Promenade de santé"], background=couleur_champ, foreground=couleur_texte)
combo_niveau_activite = ttk.Combobox(fenetre, values=["Faible(0-1/semaine)", "Moyen(2-3/semaine)", "Élevé(3 ou plus/semaine)"], background=couleur_champ, foreground=couleur_texte)
button_calculer = ttk.Button(fenetre, text="Calculer IMC et Besoins Caloriques", command=calculer_imc)
button_suggestions = ttk.Button(fenetre, text="Suggestion d'entraînement", command = lambda : suggerer_entrainement(combo_niveau_activite.get(),float(entry_poids.get())))
button_ouvrir_map = ttk.Button(fenetre, text="Ouvrir Carte", command=ouvrir_map)
label_titre = ttk.Label(fenetre, text="Sport Training", background=couleur_fond, foreground=couleur_texte,font=("Helvetica", 20))

label_resultat = ttk.Label(fenetre, text="Le résultat de l'IMC et les besoins caloriques seront affichés ici.", background=couleur_fond, foreground=couleur_texte)
# Cela place tous les champs, les labels, les bouttons et le texte afficher dans l'interface.
label_poids.place(relx=0.35, rely=0.1, anchor="center")
label_taille.place(relx=0.35, rely=0.2, anchor="center")
label_age.place(relx=0.35, rely=0.3, anchor="center")
label_lieu.place(relx=0.35, rely=0.4, anchor="center")
label_niveau_activite.place(relx=0.35, rely=0.5, anchor="center")
label_adresse.place(relx=0.35, rely=0.7, anchor="center")

entry_poids.place(relx=0.5, rely=0.1, anchor="center")
entry_taille.place(relx=0.5, rely=0.2, anchor="center")
entry_age.place(relx=0.5, rely=0.3, anchor="center")
entry_adresse.place(relx=0.5, rely=0.7, anchor="center")

label_sexe.place(relx=0.35, rely=0.6, anchor="center")
combo_sexe.place(relx=0.5, rely=0.6, anchor="center")
combo_activite.place(relx=0.5, rely=0.4, anchor="center")
combo_niveau_activite.place(relx=0.5, rely=0.5, anchor="center")
button_calculer.place(relx=0.3, rely=0.8, anchor="center")
button_suggestions.place(relx=0.7, rely=0.8, anchor="center")
button_ouvrir_map.place(relx=0.51, rely=0.8, anchor="center")
label_resultat.place(relx=0.5, rely=0.9, anchor="center")

label_titre.place(relx=0.5, rely=0.04, anchor="center")

fenetre.mainloop()

